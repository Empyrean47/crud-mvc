package ot.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import ot.dao.DAO;
import ot.dao.RecordsDAO;
import ot.model.Record;

@Repository("recordsDAO")
public class RecordsDAOImpl extends DAO implements RecordsDAO {

	@Override
	public List<Record> findRecordsById(int id) {

		Session session = getSession();

		Query<Record> query = session.createNamedQuery("get Records by id", Record.class);
		query.setParameter("userId", id);

		return query.getResultList();
	}

	@Override
	public Record findRecordById(int id) {

		Session session = getSession();

		Query<Record> query = session.createNamedQuery("get Record by id", Record.class);
		query.setParameter("recordId", id);

		return query.getSingleResult();
	}

	@Override
	public List<Record> findAllRecords() {

		Session session = getSession();
		Query<Record> query = session.createNamedQuery("get Records", Record.class);

		return query.getResultList();
	}

	@Override
	public void createRecord(Record record) {
		Session session = getSession();
		session.saveOrUpdate(record);
	}

	@Override
	public void updateRecord(Record record) {
		Session session = getSession();
		session.saveOrUpdate(record);
	}

	@Override
	public void deleteRecord(int id) {
		Session session = getSession();
		Query query = session.createNamedQuery("delete Record by id");
		query.setParameter("recordId", id);

		query.executeUpdate();
	}
}
