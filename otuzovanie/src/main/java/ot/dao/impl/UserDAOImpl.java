package ot.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import ot.dao.DAO;
import ot.dao.UserDAO;
import ot.model.User;

@Repository("userDAO")
public class UserDAOImpl extends DAO implements UserDAO {
	
	@Override
	public User findById(int id) {
		Session session = getSession();
		return session.get(User.class, id);
	}

	@Override
	public void saveUser(User user) {
		Session session = getSession();
		session.saveOrUpdate(user);
	}

	@Override
	public void deleteUserById(int id) {
		Query query = getSession().createQuery("delete from USER where id = :id");
		query.setParameter("id", id);
		query.executeUpdate();

	}

	@Override
	public List<User> findAllUsers() {
		Session session = getSession();
		Query<User> query = session.createQuery("from User", User.class);
		return query.getResultList();
	}

}
