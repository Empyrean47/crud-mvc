package ot.dao;

import java.util.List;

import ot.model.Record;
import ot.model.User;

public interface UserDAO {
	
	User findById(int id);
	void saveUser(User user);
	void deleteUserById(int id);
	List<User> findAllUsers();
}
