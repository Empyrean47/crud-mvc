package ot.dao;

import java.util.List;

import ot.model.Record;

public interface RecordsDAO {
	List<Record> findAllRecords();
	List<Record> findRecordsById(int userId);
	void createRecord(Record record);
	Record findRecordById(int id);
	void updateRecord(Record record);
	void deleteRecord(int id);
	
}
