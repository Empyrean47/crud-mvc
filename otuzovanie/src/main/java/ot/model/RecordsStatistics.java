package ot.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class RecordsStatistics {

	private List<Record> otuzovania;
	
	private int count;
	
	private double minWaterTemp;
	
	private double minAirTemp;
	
private double averageWaterTemp;
	
	private double averageAirTemp;
	
	public RecordsStatistics(List<Record> otuzovania, int count, double minWaterTemp, double minAirTemp,
			double averageWaterTemp, double averageAirTemp) {
		super();
		this.otuzovania = otuzovania;
		this.count = count;
		this.minWaterTemp = minWaterTemp;
		this.minAirTemp = minAirTemp;
		this.averageWaterTemp = averageWaterTemp;
		this.averageAirTemp = averageAirTemp;
	}

//	@JsonIgnore
	public List<Record> getOtuzovania() {
		return otuzovania;
	}

	public int getCount() {
		return count;
	}

	public double getMinWaterTemp() {
		return minWaterTemp;
	}

	public double getMinAirTemp() {
		return minAirTemp;
	}

	public double getAverageWaterTemp() {
		return averageWaterTemp;
	}

	public double getAverageAirTemp() {
		return averageAirTemp;
	}	
	
}
