package ot.model;

import java.util.List;

import ot.dao.RecordsDAO;

public class Records {

	private int userId;

	private final RecordsDAO dataSource;

	public Records(int userId, RecordsDAO dataSource) {
		super();
		this.userId = userId;
		this.dataSource = dataSource;
	}

	public Records(RecordsDAO dataSource) {
		this.dataSource = dataSource;
	}

	public List<Record> getUserRecords() {
		List<Record> otuzovania = dataSource.findRecordsById(userId);
		isEmpty(otuzovania);

		return otuzovania;
	}

	public List<Record> getRecords() {
		List<Record> otuzovania = dataSource.findAllRecords();
		isEmpty(otuzovania);

		return otuzovania;
	}

	public List<Record> get() {
		List<Record> otuzovania = (this.userId == 0) ? 
				dataSource.findAllRecords() : dataSource.findRecordsById(userId);
		isEmpty(otuzovania);

		return otuzovania;
	}

	private void isEmpty(List<Record> list) {
		if (list.isEmpty()) {
			// TODO: thorw exception
			// throw new javax.ws.rs.NotFoundException.NotFoundException("");
		}
	}
}
