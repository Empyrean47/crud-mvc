package ot.model;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import ot.configuration.LocalDateDeserializer;
import ot.configuration.LocalDateSerializer;

@Entity
@Table(name = "otuzovanie")
@NamedQuery(query = "SELECT p FROM Record p WHERE p.user.id =:userId", name = "get Records by id")
@NamedQuery(query = "SELECT p FROM Record p WHERE p.id =:recordId", name = "get Record by id")
@NamedQuery(query = "DELETE FROM Record WHERE ID =:recordId", name = "delete Record by id")
@NamedQuery(query = "SELECT p FROM Record p", name = "get Records")
public class Record {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false, unique = true)
	private int id;

	@JsonDeserialize(using = LocalDateDeserializer.class)  
	@JsonSerialize(using = LocalDateSerializer.class)
	@Column(name = "DATE", nullable = false)
	private LocalDate date;

	@Column(name = "LOCATION")
	private String location;

	@Column(name = "LENGHT")
	private double lenght;

	@Column(name = "WATER_TEMP")
	private double waterTemperature = Double.MAX_VALUE;

	@Column(name = "AIR_TEMP")
	private double airTemperature = Double.MAX_VALUE;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID", referencedColumnName = "ID")
	private User user;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public double getLenght() {
		return lenght;
	}

	public void setLenght(double lenght) {
		this.lenght = lenght;
	}

	public double getWaterTemperature() {
		return waterTemperature;
	}

	public void setWaterTemperature(double waterTemperature) {
		this.waterTemperature = waterTemperature;
	}

	public double getAirTemperature() {
		return airTemperature;
	}

	public void setAirTemperature(double airTemperature) {
		this.airTemperature = airTemperature;
	}

	@JsonIgnore
	public User getUser() {
		return user;
	}

	@JsonProperty
	public void setUser(User user) {
		this.user = user;
	}
	
	public boolean isWaterTempFilled() {
		return (waterTemperature != Double.MAX_VALUE) ? true : false; 
	}
	
	public boolean isAirTempFilled() {
		return (airTemperature != Double.MAX_VALUE) ? true : false; 
	}
}
