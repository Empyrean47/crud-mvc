package ot.model;

import java.util.List;
import java.util.NoSuchElementException;

public class RecordInfo {

	private Records dataSupply;

	public RecordInfo(Records dataSupply) {
		this.dataSupply = dataSupply;
	}

	public RecordsStatistics value() {
		List<Record> otuzovania = dataSupply.get();

		double minWater = otuzovania.stream().filter(Record::isWaterTempFilled)
				.mapToDouble(Record::getWaterTemperature)
				.min()
				.orElseThrow(NoSuchElementException::new);

		double minAir = otuzovania.stream().filter(Record::isAirTempFilled)
				.mapToDouble(Record::getAirTemperature)
				.min()
				.orElseThrow(NoSuchElementException::new);

		double averageWater = otuzovania.stream().filter(Record::isWaterTempFilled)
				.mapToDouble(Record::getWaterTemperature)
				.average()
				.orElseThrow(NoSuchElementException::new);

		double averageAir = otuzovania.stream().filter(Record::isAirTempFilled)
				.mapToDouble(Record::getAirTemperature)
				.average()
				.orElseThrow(NoSuchElementException::new);

		return new RecordsStatistics(otuzovania, otuzovania.size(), minWater, minAir, averageWater, averageAir);
	}

}
