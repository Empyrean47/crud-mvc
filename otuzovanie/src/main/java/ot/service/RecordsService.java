package ot.service;

import ot.model.Record;
import ot.model.RecordsStatistics;

public interface RecordsService {
	
	RecordsStatistics getOverallStatistics();
	RecordsStatistics findRecordsById(int userId);
	void createRecord(Record record);	
	void updateRecord(int id, Record updated);
	void deleteRecord(int id);
}
