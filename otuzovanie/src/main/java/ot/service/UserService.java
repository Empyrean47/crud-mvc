package ot.service;

import java.util.List;

import ot.model.User;

public interface UserService {
	
	User findById(int id);
	void saveUser(User user);
	void deleteUserById(int id);
	List<User> findAllUsers();
}
