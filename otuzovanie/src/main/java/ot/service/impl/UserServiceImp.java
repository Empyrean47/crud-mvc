package ot.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ot.dao.UserDAO;
import ot.model.User;
import ot.service.UserService;

@Service("userService")
@Transactional
public class UserServiceImp implements UserService {

	@Autowired
	private UserDAO dao;

	@Override
	public User findById(int id) {
		return dao.findById(id);
	}

	@Override
	public void saveUser(User user) {
		dao.saveUser(user);
	}

	@Override
	public void deleteUserById(int id) {
		dao.deleteUserById(id);
	}

	@Override
	public List<User> findAllUsers() {
		return dao.findAllUsers();
	}
}
