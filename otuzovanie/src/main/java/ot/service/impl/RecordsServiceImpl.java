package ot.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ot.dao.RecordsDAO;
import ot.model.Record;
import ot.model.RecordInfo;
import ot.model.Records;
import ot.model.RecordsStatistics;
import ot.service.RecordsService;

@Service("recordsService")
@Transactional
public class RecordsServiceImpl implements RecordsService {

	@Autowired
	private RecordsDAO dao;

	@Override
	public RecordsStatistics findRecordsById(int userId) {
		return new RecordInfo(new Records(userId, dao)).value();
	}

	@Override
	public RecordsStatistics getOverallStatistics() {
		return new RecordInfo(new Records(dao)).value();
	}

	@Override
	public void createRecord(Record record) {
		dao.createRecord(record);
	}

	@Override
	public void updateRecord(int id, Record updated) {
		if (updated == null) {
			return;
		}
		dao.updateRecord(updated);
	}

	@Override
	public void deleteRecord(int id) {
		dao.deleteRecord(id);
	}
}
