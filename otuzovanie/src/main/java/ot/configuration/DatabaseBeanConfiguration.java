package ot.configuration;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import ot.configuration.values.DataSourceProperties;
import ot.configuration.values.HibernateProperties;

@Configuration
@EnableTransactionManagement
public class DatabaseBeanConfiguration {

	private String[] modelScan = new String[] { "ot.model" };

	@Bean
	public LocalSessionFactoryBean sessionFactory(DataSource dataSource, Properties hibernate) {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(dataSource);
		sessionFactory.setPackagesToScan(modelScan);
		sessionFactory.setHibernateProperties(hibernate);
		return sessionFactory;
	}

	@Bean
	public DataSource dataSource(DataSourceProperties properties) {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(properties.getDriverClass());
		dataSource.setUrl(properties.getConnectionUrl());
		dataSource.setUsername(properties.getUsername());
		dataSource.setPassword(properties.getPassword());
		return dataSource;
	}

	@Bean(name = "hibernate")
	public Properties hibernateProperties(HibernateProperties hibernateProperties) {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", hibernateProperties.getHibernateDialect());
		properties.put("hibernate.show_sql", hibernateProperties.getHibernateShowSql());
		return properties;
	}

	@Bean
	@Autowired
	public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
		HibernateTransactionManager txManger = new HibernateTransactionManager();
		txManger.setSessionFactory(sessionFactory);
		return txManger;
	}
}
