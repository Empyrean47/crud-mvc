package ot.configuration.values;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Spring-managed Hibernate properties.
 */
@Component
public class HibernateProperties {
	/** SQL dialect */
    @Value("${ttpb.service.hibernate.dialect}")
    private String hibernateDialect;
    /** switch to show SQL statements in logs */
    @Value("${ttpb.service.hibernate.show_sql}")
    private String hibernateShowSql;

    /**
     * SQL dialect
     * @return SQL dialect to use
     */
    public String getHibernateDialect() {
        return hibernateDialect;
    }

    /**
     * Whether SQL statements should be logged.
     * @return true if Hibernate should log SQL statements.
     */
    public String getHibernateShowSql() {
        return hibernateShowSql;
    }
}
