package ot.configuration.values;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Spring-managed datasource properties.
 */
@Component
public class DataSourceProperties {

	/** JDBC driver class */
    @Value("${hibernate.connection.driver_class}")
    private String driverClass;

    /** database user name */
    @Value("${hibernate.connection.username}")
    private String username;

    /** database password */
    @Value("${hibernate.connection.password}")
    private String password;

    /** database connection url */
    @Value("${hibernate.connection.url}")
    private String connectionUrl;

	public String getDriverClass() {
		return driverClass;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getConnectionUrl() {
		return connectionUrl;
	}
}
