package ot.configuration;

import java.io.File;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

/**
 * The main Spring bean configuration point.
 */
@Configuration
@ComponentScan("ot")
public class BeanConfiguration {
	/**
	 * Loads properties
	 * 
	 * @return PropertyPlaceholderConfigurer
	 */
	@Bean
	public static PropertyPlaceholderConfigurer properties() {

		// File propsFile = new File(System.getProperty("user.home"),
		// "otuzovacka.properties");
		File propsFile = new File("otuzovacka.properties");
		PropertyPlaceholderConfigurer eppc = new PropertyPlaceholderConfigurer();
		eppc.setLocations(new FileSystemResource(propsFile));
		return eppc;
	}

}
