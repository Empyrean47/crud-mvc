package ot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ot.model.Record;
import ot.model.RecordsStatistics;
import ot.service.RecordsService;
import ot.service.UserService;

@RestController
@RequestMapping("/records")
@ComponentScan("ot")
class RecordsController {

	@Autowired
	private RecordsService recordsService;

	@Autowired
	private UserService userService;

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public RecordsStatistics getRecordsById(@PathVariable("id") int id) {
		return recordsService.findRecordsById(id);
	}

	@GetMapping(value = "/statistics", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public RecordsStatistics getOverallStatistics() {
		return recordsService.getOverallStatistics();
	}

	@PostMapping(value = "/{userId}/addRecords", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public void createRecord(@PathVariable("userId") int id, @RequestBody Record record) {
		record.setUser(userService.findById(id));
		recordsService.createRecord(record);
	}

	@PutMapping(value = "/updateRecord/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public String updateRecord(@PathVariable("id") int id, @RequestBody Record record) {
		//TODO: find by id
		//TODO: merge records
		recordsService.updateRecord(id, record);
		return "updated record" + id;
	}

	@DeleteMapping(value = "/deleteRecord/{id}")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public String deleteRecord(@PathVariable("id") int id) {
		recordsService.deleteRecord(id);
		return "deleted record" + id;
	}

}
