package ot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ot.model.RecordsStatistics;
import ot.model.User;
import ot.service.RecordsService;
import ot.service.UserService;

/**
 * Spring-based controller which will server GET/POST requests
 */
@Controller
@Deprecated
@RequestMapping("/")
public class AppController {

	@Autowired
	private UserService userService;

	@Autowired
	private RecordsService otuzovaniaService;

	@Autowired
	private MessageSource messageSource;

	@RequestMapping(value = { "/list" }, method = RequestMethod.GET)
	public String listUsers(ModelMap model) {
		List<User> users = userService.findAllUsers();
		model.addAttribute("users", users);
		return "allUsers";
	}

	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public String home(ModelMap model) {
		return "home";
	}

	@RequestMapping(value = "/otuzovaniaAll", method = RequestMethod.GET)
	public String listOtuz(ModelMap model) {
		RecordsStatistics otuzovania = otuzovaniaService.getOverallStatistics();
		model.addAttribute("otuzovania", otuzovania);
		return "OtuzovaniaForAllUsers";
	}
	
	@RequestMapping(value = "/otuzovania", method = RequestMethod.GET)
	public String listOtuzovania(ModelMap model) {
		RecordsStatistics otuzovania = otuzovaniaService.findRecordsById(1);
		model.addAttribute("otuzovania", otuzovania);
		return "OtuzovaniaById";
	}

}
