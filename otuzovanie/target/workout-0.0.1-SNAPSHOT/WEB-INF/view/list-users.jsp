<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>

<html>

<head>
<title>List Customers</title>

<!-- reference our style sheet -->

<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/style.css" />

</head>

<body>

	<div id="wrapper">
		<div id="header">
			<h2>URM - User Relationship Manager</h2>
		</div>
	</div>

	<div id="container">

		<div id="content">

			<!--  button -->
			<input type="button" value="Add user"
				onclick="window.location.href='showFormForAdd'; return false;"
				class="add-button">


			<!--  add our html table here -->

			<table>
				<tr>
					<th>Name</th>
					<th>Height</th>
					<th>Weight</th>
					<th>Bodyfat</th>
					<th>Action</th>
				</tr>

				<c:forEach var="tempUser" items="${users}">
					<c:url var="updateLink" value="/user/showFormForUpdate">
						<c:param name="userId" value="${tempUser.id}"></c:param>
					</c:url>

					<tr>
						<td>${tempUser.nickname}</td>
						<td>${tempUser.bodyStatistics.height}</td>
						<td>${tempUser.bodyStatistics.weight}</td>
						<td>${tempUser.bodyStatistics.bodyfat}</td>
						<td><a href="${updateLink}">Update</a></td>
					</tr>

				</c:forEach>

			</table>

		</div>

	</div>


</body>

</html>









