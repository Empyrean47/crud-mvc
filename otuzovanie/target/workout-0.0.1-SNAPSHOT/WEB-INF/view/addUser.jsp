<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>

<head>
	<title>Save User</title>

	<link type="text/css"
		  rel="stylesheet"
		  href="${pageContext.request.contextPath}/resources/css/style.css">

	<link type="text/css"
		  rel="stylesheet"
		  href="${pageContext.request.contextPath}/resources/css/addUserStyle.css">
</head>

<body>
	
	<div id="wrapper">
		<div id="header">
			<h2>URM - User Relationship Manager</h2>
		</div>
	</div>

	<div id="container">
		<h3>Save User</h3>
		
		<!-- mapped to userController -->
		<form:form action="saveUser" modelAttribute="user" method="POST">
		
			<form:hidden path="id" />
		
			<table>
				<tbody>
					<tr>
						<td><label>Nickname:</label></td>
						<td><form:input path="nickname" /></td>
					</tr>
				
					<tr>
						<td><label>Height:</label></td>
						<td><form:input path="bodyStatistics.height" /></td>
					</tr>

					<tr>
						<td><label>Weight:</label></td>
						<td><form:input path="bodyStatistics.weight" /></td>
					</tr>
					
					<tr>
						<td><label>Bodyfat:</label></td>
						<td><form:input path="bodyStatistics.bodyfat" /></td>
					</tr>

					<tr>
						<td><label></label></td>
						<td><input type="submit" value="Save" class="save" /></td>
					</tr>

				
				</tbody>
			</table>
		
		
		</form:form>
	
		<div style="clear; both;"></div>
		
		<p>
			<a href="${pageContext.request.contextPath}/user/list">Back to List</a>
		</p>
	
	</div>

</body>

</html>










